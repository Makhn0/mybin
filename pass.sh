#!/bin/bash
function kl (){
    kpcli --kdb=$HOME/dox/sync/PW.kdbx  --pwfile $tempfile ;
}
function kls(){
    kl --command "ls $1" | awk '/== Groups ==/,0' |sed -r "s/==.*==//" |sed -r "/^\s$/d" ;
}
function kl_pass(){
    kl --command "show -f $1" ;
}
function main(){
    tempfile="$HOME/pw"
    if  [[ -z "$(cat $tempfile)" ]]; then
        kl --command "ls"  || ( printf "enter master password:" && \
                                    read  -s pw)
    fi
    printf "$pw">$tempfile
    while true; do
        # select="$(kls "$dir" | myrofi -dmenu -p "$prompt" )"
        select="$(kls "$dir" | dmenu  -p "$prompt" )"
        dir="$select"
        [[ $? == "0" ]] || break
        echo $select
        kl_pass $select
        break
    done
    echo "$select"
}

echo "WIP do not use"
