#!/usr/bin/gnuplot --persist

set terminal qt
set xdata time
set ytics offset -1,0
set ylabel "kg" tc rgb "#00ffffff"
set grid ls 2 lc rgb "#000fffff"
set key left above tc rgb "#00ffffff" box lc rgb "#00ffffff"


set ydata
#set style increment userstyles
set size 1, 1
set border lt rgb "#22ffffff"
set offsets 0.2,0.2,0.2,0.2
set bmargin at screen 0.1
set style line 1 lc rgb 'green'
set style line 2 lc rgb 'purple'
set timefmt "%Y-%m-%dT%H:%M"

bind all "d" "e=0 ; exit gnuplot"

#plot "~/dox/temp" using 1:2 with points pt 7 ps 2.5 title "My Weight", \
show margins
plot main_file using 1:2 with points pt 7 ps 2.5 lc 'green' title "My Weight" , \
ave_file using 1:2:3 w errorlines pt 2 ps 4.5 title "Daily Ave"
 replot

e=1

while (e) {
replot
pause 1
}
