# root="/e/media/music"
# chime_dir="/Devo - Discography/Oh,No!It's-Devo"
# chime_file="01 Time Out for Fun.mp3"
# chime_path="${chime_dir}/${chime_file}"
# just use findadd
chime_song="Time Out for Fun"
find_title_re="(?<=- ).*$"
current_song_file=~/.pomocurrentsong
function resume(){
    mpc clear
    mpc load pomosave
    mpc searchplay Title "$(cat ${current_song_file})"
}
function chime(){
    mpc rm pomosave
    mpc save pomosave
    mpc current | grep -oP "$find_title_re"> $current_song_file

    mpc clear
    mpc findadd Title "$1"
    mpc play

}

while [[ $go -ne "y" || -z $go ]];
do
    if [[ -z ${1} ]]; then
        min=25
    else
        min=${1}
    fi



    seconds=$((60*${min}))
    intervals=$((${seconds}/300))
    for (( i = 0; i <= $intervals; i++))
    do
        echo $((${min}-i*5)) minutes left...
        sleep 300
    done
    chime "$chime_song"
    # sleep 2 && chime "$chime_song"


    echo "continue pomo?"
    read go 
    echo "continue music?"
    read music 
    if [[ $music == "y" ]] ; then
        resume
    fi

done
