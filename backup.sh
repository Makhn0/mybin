#!/bin/sh
#apparently there is a backup.sh in the gnu tar packagenotify-send "backing up"
#vps=root@emaher.xyz::backup

drive=/e
thumb=/mnt/usb
#homeToVps=( code bin games/roms )
homeToE=(pix stowdots code bin dox mail games/roms)
function backup(){
    echo "backing up ${1} to ${2}" |tee >> /tmp/backuplog
    rsync -raAXvz ${1} ${2}
    echo $? |tee >> /tmp/backuplog

}

crontab -l > ~/sysdots/$(whoami).$(hostname)_cron && notify-send "crontab backed up"
    # backup to sync

# backup to thumb
#[ -d $thumb ] && backup  --modify-window=1 $sync/.password-store/ /mnt/usb/.password-store
#backup dox/.password-store /mnt/usb/
# back up to vps
#for i in ${homeToVps[*]} ;
#do
#    backup ~/$i $drive/backups/
#done && notify-send "backuped to vps complete" || notify-send "backup-failed"

echo backing up $(date) >> /tmp/backuplog
[ "$1" != q ] && backup ~/pix $drive/backups/pix
if [[ -d $drive ]]; then
    for i in ${homeToE[*]} ;
    do
        backup ~/$i $drive/backups/
    done && notify-send "backup to E drive complete" || notify-send "backup-failed"

fi

