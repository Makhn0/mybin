#!/usr/bin/env sh

start_wob(){
    WOBCONFIG=$HOME/.config/wob/wob.ini
    WOBFIFO=/tmp/wobpipe
    [ -p $WOBFIFO ]|| mkfifo $WOBFIFO
    tail -f $WOBFIFO | wob -c $WOBCONFIG &
    }

case $1 in
    -r) killall wob
        start_wob
        ;;
    -s) start_wob ;;
    -k) killall wob;;
    *) start_wob;;
esac
