#!/bin/zsh

[[ "$1" == "inc" ]] && \
      	amixer -q -D pulse sset Master ${2:-5}%+ 

[[ "$1" == "dec" ]] && \
       	amixer -q -D pulse sset Master ${2-5}%- 
[[ "$1" == "toggle" ]] && amixer -q -D pulse sset Master toggle

[[ "$1" =~ "[0-9]+" ]] && amixer -q -D pulse sset Master ${1}%

    vol=$(amixer get Master | tail -n 1 |sed "s/.*://g" |awk '{print $3}' | tr -d "[]%")
    vol_status=$(amixer get Master | tail -n 1 |sed "s/.*://g" |awk '{print $4}' | tr -d "[]%")

[ $vol_status = "on" ] || mute="muted"

echo ${vol} ${mute}
