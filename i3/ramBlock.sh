#!/bin/bash
ram="$(bc <<< "scale=1; $(free | sed -n '2,2p' | awk '{ print $3 }')*100 / $(free | sed -n '2,2p' | awk '{ print $2 }')")"%
swap="$(bc <<< "scale=1; $(free | sed -n '3,3p' | awk '{ print $3 }')*100 / $(free | sed -n '3,3p' | awk '{ print $2 }')")"
        # && [[ ! $a -eq 0 ]] && echo " $a"

# echo swap=$swap
# echo ram=$ram
if  (( $(echo "$swap > 0 " | bc -l) )); then
    if [[ ! -f $HOME/.swap_use ]]; then
        notify-send "aww shit swap is being used ${swap}%"
        touch $HOME/.swap_use
    fi
      swap="SWAP ${swap}%" 
else
    rm ~/.swap_use
    unset swap
fi



echo "$ram $swap"
