#!/bin/bash
WINEPREFIX="$HOME/Games-Wine/LoL"
WINEARCH="win32"
DOWNLOADS="$HOME/Downloads"
INSTALLER="League of Legends installer NA.exe"
INSTALLER="Install League of Legends na.exe"

#WINE_LOL_GIT=https://github.com/dreadsend/wine-lol
# install neccesary glibc
read -p "WINEPREFIX=$WINEPREFIX ?" prefix
if [[ ! $prefix =~ "y" ]]; then
#WINE_LOL_GIT=https://github.com/dreadsend/wine-lol
#
## based on 
# https://www.reddit.com/r/leagueoflinux/comments/egfykt/guide_instalation_and_optimalization_on_arch/
#
# install neccesary glibc
	read -p "WINEPREFIX:" WINEPREFIX
fi
read -p "rebuild wine-lol-glibc etc.?" re

if [[ $re == y	]]; then
	yay -Syyu 
	yay -S wine-lol-glibc \
     		wine-lol  
fi


# wine dependency hell
read -p "run Wine dependency hell script?" re

if [[ $re == y	]]; then
	windep.sh
fi

# ask to reset Wine prefix
read -p "erase wine prefix?" erase
if [[ $erase == y ]]; then
	rm -rf "$WINEPREFIX" 
fi

# Configure wineprefix
read -p "configure wine prefix?" re
if [[ $re == y ]]; then
	mkdir -p $WINEPREFIX
	WINEPREFIX=$WINEPREFIX WINEARCH=$WINEARCH /opt/wine-lol/bin/winecfg
fi
# 
# mv league installer into wine prefix 
if [[ ! -f "$DOWNLOADS/$INSTALLER" ]]; then
	echo not downloaded
else
	echo installer found: $DOWNLOADS/$INSTALLER 
fi
while [[ ! -f "$DOWNLOADS/$INSTALLER"  && con != y ]]; do

read -p "Download league client from website or press y to skip " con

done

read -p "copy installer to wine prefix? " con
if [[ $con == y ]]; then
	cp "$DOWNLOADS/$INSTALLER" "$WINEPREFIX/drive_c/$INSTALLER"
fi

# install league in wine prefix
read -p "run installer?" con
if [[ $con == y ]]; then
	WINEPREFIX=$WINEPREFIX WINEARCH=$WINEARCH /opt/wine-lol/bin/wine "$HOME/Games-Wine/LoL/drive_c/$INSTALLER"
fi

# run the game
read -p "run Game?" con
if [[ $con == y ]];  then
	  WINEPREFIX=$WINEPREFIX WINEARCH=$WINEARCH /opt/wine-lol/bin/wine "$HOME/Games-Wine/LoL/drive_c/Riot Games/League of Legends/LeagueClient.exe"
fi
