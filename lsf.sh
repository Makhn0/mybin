#!/bin/sh
as_root ()
{
	  if [ $EUID = 0 ]; then $*
	  elif [ -x /usr/bin/doas ]; then doas $*
	  else su -c \\"$*\\"
	  fi
}

lfsget (){ wget $1 --directory-prefix=/src && tb=${1##*/} && cd /src && tar xvf $tb && cd ${tb%%.tar*} && pwd && echo  "md5sum:$(md5sum /src/$tb)"  ; }
