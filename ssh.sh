#!/bin/bash
host_port="$(cat ~/.ssh/known_hosts | grep -e "^\[" | grep -oP -e "^.*,"| tr -d [], |myrofi -dmenu )"
host=${host_port%%:*}
port=${host_port#*:}
echo $host
echo $port
cmd="ssh $host -p $port; exec </dev/tty"
st -e "bash -i <<< $cmd" 
