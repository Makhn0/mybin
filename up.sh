  #!/bin/bash
  #export DISPLAY=:0
  #export XAUTHORITY="${HOME}/.Xauthority"
  notify-send "upgrading packages" "yay -Syyu"
  r=$(yay -Syyup --print-format "%n"  |  grep -oP "^linux$")
  echo debug:r=$r
  yay -Syyu $@
  #if [ $r  ]; then
      #yesno.sh "Kernel updated. Ready to reboot?" "dmenu -P -p 'reboot' | $sudo -S reboot"
  #fi
  notify-send "cleaning repos" "$cleaning orphans"
  sudo yay -Rns $(yay -Qtdq)
  notify-send "cleaning repos" "$sudo paccache -r"
  sudo paccache -r
  sudo pacman -Sc
#  notify-send "cleaning repos" "$rmlint"
#  rmlint
#  notify-send "cleaning repos" "$rmlint.sh"
#  ./rmlint.sh

  notify-send "updating emacs" "doom update"
  ~/.emacs.d.doom/bin/doom update
  notify-send "updating emacs" "doom sync"
  ~/.emacs.d.doom/bin/doom sync

  notify-send "Upgrade Complete"
  echo "updated @ $(date)">> ~/.update.log
