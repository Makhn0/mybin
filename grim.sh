#!/usr/bin/env sh

GRIM_DIR=$HOME/pix/grim
bywindow(){
    #TODO
    #To capture a window
g="$(hyprctl activewindow |\
    awk '/at:/{printf $2;printf " "}
         /size:/{str=$2;sub(",","x",str);print str}
')"
title="$(hyprctl activewindow | awk '/title:/{print $NF}')"
date="$(date '+%F_%T').png"
filename=$GRIM_DIR/${title}:${date}
 [ $filename = "nil"] && exit
grim -g  "$g" $filename && notify-send "window $title saved to $filename"

}

slurp_bemenu(){
#break this up into Just the slurp/grim section, and then a previwew
    area=$(slurp)
    filename=$GRIM_DIR/$(echo  | bemenu -p "$GRIM_DIR/{filename}.png:").png
    [ $filename = "nil" ] && exit && notify-send "canceled"
    echo here mb moyu >> /dev/stderr
       grim -g "$area" "$filename"
       echo $filename
}

slurp_clipboard(){
grim -g "$(slurp)" - | wl-copy && notify-send "region copied to clipboard"
}

img2clip(){

    #

    fn=$GRIM_DIR/$(ls $GRIM_DIR/ | bemenu )
    cat $fn | wl-copy && notify-send "copied $fn to clipboard"

}

case $1 in
    -c)  #clip an area of thes screen
         slurp_bemenu ;;
    -v) #clip area of screen and then display the file in nsxiv
         nsxiv $(slurp_bemenu) ;;
    -b) # send an area of the screen to the clipboard
         slurp_clipboard ;;
    -B) # send a screen shot to the clipboard
	# need to remember how I installed this
         img2clip ;;
    -w) # capture a window
       bywindow ;;
       -s) # show a captured image in nsxiv
        nsxiv $(ls $GRIM_DIR/ | bemenu) ;;
esac
