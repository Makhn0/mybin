#!/bin/sh

[[ $(hostname) = lappy9000 ]] && bat=$(acpi -b | awk -F'[,:%]' '/Battery 1:/{print $3}' | cut -b2-  )

if [[ $1 = -d ]]; then
	if [[ $bat -lt 5 ]];then
	dunstify "Battery $bat <5%" -u critical -a battery
	fi
	if [[ $bat -lt 20 ]]; then
	dunstify "Battery $bat <20%" -u normal -a battery
	fi
else
	echo $bat
fi
