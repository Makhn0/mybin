#!/bin/sh
pstree -lp | grep -- -dwm_status.sh\([0-9] | sed "s/.*sleep(\([0-9]\+\)).*/\1/" | xargs kill
if [[ $1 == "-k" ]]; then
    killall dwm_status.sh
    dwm_status.sh 2>&1  > /dev/null & disown
fi
